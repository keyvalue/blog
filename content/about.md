+++
date = "2019-03-10"
draft = false
title = "About"
menu = "main"
+++

I'm a software developer living and working in Brooklyn, New York. I have a 
focus on backend software and the systems and infrastructure that support it.
That said, I still love the web, and I am pretty excited about WASM and its
possibilities.

I believe

* the purpose of software is to make life easier, and this is chiefly 
  accomplished by automating manual, error-prone, or toilsome tasks
* working code is better than theoretical discussion
* in trying something out and seeing if it works. If it doesn't, you still probably learned something

You can follow me on GitHub at [anxiousmodernman](https://github.com/anxiousmodernman) and [dontlaugh](https://github.com/dontlaugh),
and on GitLab as [keyvalue](https://gitlab.com/keyvalue). I also hang out on the
RedoxOS Mattermost server, and the Redox [GitLab server](https://gitlab.redox-os.org/coleman).
I also started [git.coleman.codes](https://git.coleman.codes) to scratch my 
itch of hosting a cgit frontend. CGI is still cool.

Feel free to get in touch.

