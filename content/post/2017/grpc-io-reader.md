+++
date = "2017-08-17"
title = "Wrapping a gRPC stream with io.Reader"
description = "Turning one streaming interface into another."
draft = true
+++


Broadly, we need to do the following:

* avoid blocking
* stream things to disk

Caveats:
* The calls to Read here expect a specific type
* While reading, the `io.Reader` must be the sole user of the stream. Sharing
  the stream somewhere else while the reading would be **bad**

Our type that will implement `io.Reader` is a struct with four fields. We must 
hold the gRPC stream itself. Also we will use a `*bytes.Buffer` as a small 
internal buffer where we will temporarily store data from gRPC. Users of our 
custom type will be drawing from this internal buffer whenever they call `Read`.
We will also hold two booleans to help us maintain our internal state: one bool
will be marked `true` when gRPC gives us `io.EOF`. Another bool will be marked 
`true` when our own internal buffer is empty **and** there is no more data from
the gRPC stream.

This final state means we will return `EOF` to the caller of our reader. That's
the signal to the caller that they should stop reading.

```go
type PutStreamAdapter struct {
    stream  GMData_PutStreamServer
    buf     *bytes.Buffer  
    grpcEOF bool
    drained bool
}

func NewPutStreamAdapter(s GMData_PutStreamServer) *PutStreamAdapter {
	var psa PutStreamAdapter
	psa.stream = s
	psa.buf = bytes.NewBuffer(make([]byte, 0))
	return &psa
}
```

And our implementation of the interface

```go
func (psa *StreamAdapter) Read(b []byte) (int, error) {
	var n int

	// first, check our internal buffer for contents. If they exist, read from there.
	if psa.buf.Len() > 0 {
		var err error
		n, err = psa.buf.Read(b)
		if err == io.EOF {
			if psa.drained {
				return 0, io.EOF
			}
			if psa.grpcEOF {
				psa.drained = true

				// We are at the end of the grpc stream, AND we have drained our
				// internal buffer, return EOF to our consumer.
				if n != 0 {
					panic("expected internal buffer to be empty")
				}
				return n, err
			}
			// We are not at the end of our grpc stream. Do not return EOF to our
			// consumer. They should take n bytes from b and call Read again.
			psa.buf.Reset()
			return n, nil
		}
		if err != nil {
			psa.buf.Reset()
			return n, err
		}
		return n, nil
	}

	frg, err := psa.stream.Recv()
	if err != nil && err != io.EOF {
		// Some kind of grpc stream error has occured.
		return 0, err
	}

	if err == io.EOF {

		psa.grpcEOF = true
		if psa.buf.Len() == 0 {
			// End of grpc stream, and our buffer is empty. Return EOF.
			return 0, io.EOF
		}
		// delegate to our buffer's read. This might return EOF, or the caller might
		// call Read again.
		return psa.buf.Read(b)
	}

	// Cast our StreamFragment to the expected type.
	ch, ok := frg.Msg.(*StreamFragment_Chunk)
	if !ok {
		t := reflect.TypeOf(frg.Msg)
		return 0, fmt.Errorf("expected *StreamFragment_Chunk type but got %v", t.Name())
	}
	// Write our data to our internal bytes.Buffer, which grows automatically.
	_, err = psa.buf.Write(ch.Chunk.Data)
	if err != nil {
		return 0, err
	}

	// Read immediately from our buffer
	return psa.buf.Read(b)
}

```


