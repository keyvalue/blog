+++
date = "2015-05-16"
draft = false
title = "A basic tmux bash script explained"

+++

Since I've been writing more Go, I've been using a lot more 
plain text editors such as vim and Sublime Text. I tried to
use an IDE for a while (IntelliJ with Go plugins), but it
felt antithetical to the raw, low-level nature of Go. Also,
the tooling wouldn't let me set my `$GOPATH` in an easy way.

So, back to text editors and shells for me.

### Scripting tmux

Many people recommend the **tmuxinator** Ruby gem to configure
tmux sessions, but I'm trying to avoid a dependency on Ruby, so
I'm going to use raw bash scripts for launching tmux sessions.

I don't need to do anything too complicated. Here are my goals:

* Launch a session with a name
* Create two windows, each with names
* In the first window (at index `0`) split into 3 panes with a sideways T shape (like `|-`)
* In the second window (index `1`), split into panes with a vim session on the left and a vim session on the right
* Back in the first window, send a `tail` command to the upper-right pane, to start monitoring a specific log file
* Attach to the session, viewing the first window

Here is the bash script that achieves this:

```bash
#!/bin/sh

tmux new-session -d -s dev-session
tmux select-window -t 0
tmux rename-window Shells
tmux new-window -n 'Editors' 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
tmux select-window -t dev-session:Shells
tmux split-window -h 'tail -f $GOPATH/src/github.com/anxiousmodernman/dirz/dirz.log'
tmux split-window -v
tmux select-window -t dev-session:Editors
tmux split-window -h 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
tmux select-window -t dev-session:Shells
tmux -2 attach-session -t dev-session
```

### Breaking it down

First, we make a new session

```bash
tmux new-session -d -s dev-session
```

Next, we can target the first (and only) existing default window in the session,
and rename the window to "Shells".

```bash
tmux select-window -t 0
tmux rename-window Shells
```

Then, spawn a new window. Give it the name 'Editors' by passing the `-n` flag, and execute `vim` as the command. The easiest way (it seems to me)
to get tmux to run a command in a window or pane is to pass it as the final argument as a string **during the creation of that window or pane**.

```bash
tmux new-window -n 'Editors' 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
```

Switch back to the "Shells" window with the `select-window` command, followed by the `-t` (target) flag, and then tmux-ish namespace path with the format
`session-name:window-name`. Then I immediately split this window into left-and-right panes with `split-window -h`. Note that the new pane, which will
appear on the _right_ has focus after it is split, and to achieve my goal of tailing a file I pass that new pane the `tail` command with arguments.

```bash
tmux select-window -t dev-session:Shells
tmux split-window -h 'tail -f $GOPATH/src/github.com/anxiousmodernman/dirz/dirz.log'
```

Then, do another split. This time a `-v` split _while the right pane has focus_. This will give us two panes on the right half of the screen
and one tall pane in the left, achieving the sideways T shape (`|-`) I want.

```bash
tmux split-window -v
```

We are done with the 'Shells' window. Next we switch back to 'Editors', and do our final split, again running vim with the same directory as a target.

```bash
tmux select-window -t dev-session:Editors
tmux split-window -h 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
```

Since vim targeted a directory both times, the default nerd tree directory view is what we see when we first view the 'Editors' window.

Finally, we focus back on 'Shells', and attach to our newly-created tmux session!

```bash
tmux select-window -t dev-session:Shells
tmux -2 attach-session -t dev-session
```

You can change the paths in my shell script example and give it a shot. 
