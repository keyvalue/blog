+++
date = "2014-12-23"
draft = false
title = "Run a PowerShell script after a Visual Studio build"

+++

Microsoft's docs are lacking on the simple how-to stuff, so I'm going to make a contribution here. This took too long for me to figure out, but I was helped by [this Stack Overflow post](http://stackoverflow.com/questions/6500320/post-build-event-execute-powershell).

If you want to simply execute a PowerShell script after a build in Visual Studio, here's how. I'm using Visual Studio 2013.

### Step 1: Find your PowerShell executable

You will need to know this information later, so open a PowerShell command line and simply type ```$PSHOME``` to print out the contents of that environment variable.

```powershell
PS> $PSHOME
C:\Windows\System32\WindowsPowerShell\v1.0
```

Note that **C:\Windows\System32\WindowsPowerShell\v1.0** is the location of the **powershell.exe** executable. It might be different on your machine.

### Step 2: Open the Build Events dialog for your Project

Visual Studio organizes build events at the project level, *not* the solution level. Navigate to your custom build events like this:

* Open Visual Studio and right click on your project.
* Click **Properties**
* Click the **Build Events** tab

You will see two big text boxes, **Pre-build event command line** and **Post-build event command line**. For new projects these should be empty. I wanted to run a script *after* my build, so I will type into the latter.

### Step 3: Add your script to the build event command line

In the big empty text box, add the command of the following format (be sure to use those quotes):

```
"C:\Path\to\powershell.exe" -file "C:\Other\path\to\custom-script.ps1"
```

The first half is the path to **powershell.exe** you located earlier. Then, you have the ```-file``` argument, and finally your custom script. For me, the specific command was:

```
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -file "C:\Tools\load-fetcher-tools.ps1"
```

Hope that helps!
