+++
date = "2016-01-10T21:06:00"
description = "Get yourself coding when you don't exactly know what you're doing."
title = "Enter the Abyss"

+++


A good tip from David Allen's *Getting Things Done*: you will find yourself more
motivated to exercise *after* you put your workout clothes on. So, no matter what,
just put on your workout clothes. The general concept is, do one physical, worldly thing, no matter
how trivial, that creates the space where achieving your goal is easier.

If your goal is related to programming, these are my favorite actions to get
yourself moving.

## Open a terminal and switch to the right directory

Where is your code? Is it in **~/Code/myproject**? Perhaps it's in **C:\Users\samantha\newsite**?
Wherever it is, open the terminal and *go there*. List the contents of the directory.
Just go look.

## Open the project in a text editor

It doesn't matter which editor, although I'm partial to Sublime or Atom because
those editors show a directory tree. It sounds trivial, but you can't read or
write code without an editor of some kind. Besides, if you've navigated to the
right directory, all you have to do next is run

```
atom .     
```

or equivalent, and you're already further along than you were.

If your project has more than one root folder, use your editor's feature that
adds additional root folders into your workspace.

## Run the tests

Find out how to run tests for your project, and run them. Maybe you run them from
the command line. Or, maybe you run them from within your editor. Just run them.

If you expect test failures, that's fine. Running tests and seeing exactly what fails
is the first step to writing the code that makes tests pass.

If your project does not have any tests, then your first order of business should
be figuring out how to write them in the language of your choice.

If your project has tests, but those tests are annoying to set up and run, fix that.
Either reach out to the person responsible and let them guide you. Better yet, offer to fix it yourself
and send them a PR.

## Read the tests

Reading tests is a good way to approach a project that has mostly been written by
someone other than yourself. It's a good way to familiarize yourself with a
codebase.

Try to find higher-level tests, or integration tests, and start with those.
These tests are probably in folders or files with names like "server", "service",
"client", or "api". They're going to exercise different parts of the system. They
might hit an external API or a database.

If you don't have access to the external API or have the appropriate test database
set up, then you need to find out how to do that. This might have been a reason
for test failures.

## Open the documentation

If your project has documentation, open it. Maybe it's a portion of your corporate
wiki, or maybe it's just a README file in the root of your project.

**Beware!** Documentation can get radically out of sync with the code's reality
very quickly. This is even the case for high-profile projects backed by billion-dollar
corporations. I am a huge advocate of docs, but you should approach them with an
attitude akin to *trust, but verify*.

If you do find an error, report it to the maintainer, or fix it yourself.

Chances are your project is using several third party libraries that have documentation
sites of their own. Open those in their own browser tabs, and bookmark them.

An especially good thing to bookmark are documentation pages that simply list
functions available in the dependency's API. [An example from the popular JavaScript
library underscorejs is here](http://underscorejs.org/). A simple **Ctrl + F** find
on this page will let your zero in on exactly what you need.

## Find the "main" method

You might know how to run the tests, but do you know how the code actually runs
in production? Most languages have an obvious way to specify the entrypoint into
a program. Often it's a function or method literally named **main**. In dynamic
languages like Python or JavaScript it is usually a specific script, with a name
like **app.js** or **main.py** or similar. It is probably documented. Find out.

## Find the application's configuration

Most projects will provide a way to inject dynamic configuration on startup. This
is because you want to be able to point your apps at a test database while testing,
and a production database while... productioning?

Configuration can be provided to an app in many ways:

* Configuration file(s) written in JSON, YAML, Java-esque ".properties" files, etc.
* Shell environment variables that the application expects to be set.
* Flags passed to the application at the command line, e.g. `app.py --test-db="127.0.0.2"`

There are a finite number of configurable values for any app. Find these configurable
values and understand which ones you need to set or modify.

## Read and/or write issues/requirements

If you're working by yourself, make sure you have *very specifically* written down
exactly what you want your application to do. The more specific you are, the easier
it is to translate these requirements into code.

If you're working on a codebase with a team, read the issues that are sitting in
your team's issue tracker (whether it's JIRA, GitHub issues, or whatnot). Find
the high level goals, milestones, bugs and conversations. Link these back to the
parts of the code under discussion. Dive deep.

If the issue tracker is a mess, don't tell your project manager that they're bad at their
job. Instead, politely ask the most effective and helpful way to submit an issue
if you find one. Say something like, "How would I submit different kinds of issues?
Say, a legitimate bug vs. an update to some out of date documentation?" That will
give you better insight into the process that *should* be adhered to, even if it's
not perfectly applied.

## Script an annoying command

If you find yourself typing a long command over and over, write a script. For
instance, to launch a test server for this blog, I need to run:

```
hugo server --theme=trill --buildDrafts=true --buildFuture=true --watch=true -p 8080
```

So instead I just wrapped it in a small script.

```bash
#!/bin/bash

hugo server --theme=trill --buildDrafts=true --buildFuture=true --watch=true -p 8080
```

Then I just have to run:

```
./runserver.sh
```

Bonus points: scripts can serve as documentation for your own personal projects.
Never shy away from writing small scripts that automate trivial stuff. If something
is difficult to automate via scripts, it's actually important to find out why,
and work towards fundamental changes to your project that will enable automation
to be easier.

## Go forth into the abyss

You have to just *get in there* and figure stuff out. That's what I call the abyss.
There is no amount of reading or learning or toy problem-solving that can replace
reading and writing real code that does real stuff. It's messy, it's complicated.
There's bugs and lots of logic. Dive in, take notes, ask questions, and tinker.

----

I hope these action items help you get unstuck if you don't know where to start.
Often I find these tips to be helpful if I'm coming back to a side project that
I've abandoned for a few weeks.

The common thread between all these things is that they are code-focused. If you
already have a little code written, keep going and solve small practical problems
until you deliver bigger and bigger solutions. Resist the urge to wander off and
read random blogs like this one. Stay with your code.
