
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-us">
<head>
  <link href="http://gmpg.org/xfn/11" rel="profile">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  
  <meta content="timers, systemd, cron" name="keywords">
  

  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <title> Systemd Timers &middot; coleman.codes </title>

  
  <link rel="stylesheet" href="https://coleman.codes/css/poole.css">
  <link rel="stylesheet" href="https://coleman.codes/css/syntax.css">
  <link rel="stylesheet" href="https://coleman.codes/css/hyde.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface">

  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144-precomposed.png">
  <link rel="shortcut icon" href="/favicon.ico">

  
  <link href="" rel="alternate" type="application/rss+xml" title="coleman.codes" />

  
  <link rel="stylesheet" href="https://yandex.st/highlightjs/8.0/styles/default.min.css">
  <script src="https://yandex.st/highlightjs/8.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>


<body>

  <div class="sidebar">
  <div class="container sidebar-sticky">
    <div class="sidebar-about">
      <h1>Coleman</h1>
      <p class="lead"></p>
    </div>

    <ul class="sidebar-nav">
      <li><a href="/">Home</a> </li>
      
        <li><a href="/about/"> About </a></li>
      
    </ul>

  </div>
</div>


  <div class="content container">
    <div class="post">
      <h1>Systemd Timers</h1>
      <span class="post-date">Sat, Dec 2, 2017</span> 

<p>Systemd is the standard service manager and init process for modern Linux. It
unifies many system utilities under a new suite of tools, daemons, and APIs. If
you want to enable services to start at boot, and restart themselves when they
crash, you use systemd.</p>

<p>Systemd also provides a way to run commands on a schedule, just like the venerable
<code>crond</code>. This feature is known as &ldquo;timers&rdquo; in systemd. Let&rsquo;s look at how they work.</p>

<h2 id="check-active-timers">Check Active Timers</h2>

<p>First off, it&rsquo;s likely that your Linux distribution, or a package you&rsquo;ve
installed, has already created some systemd timers. You can list them like this.</p>

<pre><code>sudo systemctl list-timers
</code></pre>

<p>This will yield a list of <strong>active</strong> timers.</p>

<pre><code class="language-txt"># some output elided
NEXT        LEFT     LAST  PASSED   UNIT             ACTIVATES
Sat 2018..  3h 38min Fri.. 20h ago  logrotate.timer  logrotate...
Sat 2018..  3h 38min Fri.. 20h ago  man-db.timer     man-db.ser..
Sat 2018..  3h 38min Fri.. 20h ago  shadow.timer     shadow.ser..
Sat 2018..  15h left Fri.. 8h ago   systemd-tmpfiles systemd-tm..

5 timers listed.
Pass --all to see loaded but inactive timers, too.
</code></pre>

<p>We show 4 active timers, one per row. The columns are fairly self-explanatory.
&ldquo;NEXT&rdquo; and &ldquo;LEFT&rdquo; are human-readable times when the timer will fire next. &ldquo;LAST&rdquo;
and &ldquo;PASSED&rdquo; are the opposite: the date and time of the last timer run. &ldquo;UNIT&rdquo;
and &ldquo;ACTIVATES&rdquo; indicate the two files systemd requires for a timer. We should
consider these two files next.</p>

<h2 id="required-files">Required Files</h2>

<p>To make a working systemd timer, at least two files are required. The first is
a file with a <strong>.timer</strong> extension. The other is usually some kind of service
file, commonly with a <strong>.service</strong> extension. Importantly, they must both share
a name, with only the extensions being different. So for an <strong>example.timer</strong> a
corresponding <strong>example.service</strong> must exist.</p>

<p>Note that, in systemd terms, both files are known as &ldquo;unit files&rdquo;. The thing that
makes timers special are 1) their .timer extension and 2) the fact that they
can activate other unit files. This is the meaning of the &ldquo;UNIT&rdquo; and &ldquo;ACTIVATES&rdquo;
columns above.</p>

<p>Both kinds of unit files can usually be found at two common paths</p>

<ul>
<li><strong>/usr/lib/systemd/system</strong> for distro or package-provided timers and services</li>
<li><strong>/etc/systemd/system</strong> for your own custom timers and services</li>
</ul>

<p>You will probably want to create your timers under <strong>/etc/systemd/system</strong>.</p>

<h3 id="example-foo-timer-and-foo-service">Example foo.timer and foo.service</h3>

<p>To demonstrate a timer that runs 15 minutes after boot and then once every 30
minutes the system is active, see the following <code>OnBootSec=</code> and
<code>OnUnitActiveSec=</code> configuration option under <code>[Timer]</code>.</p>

<pre><code># /etc/systemd/system/foo.timer
[Unit]
Description=An example timer

[Timer]
OnBootSec=15min
OnUnitActiveSec=30min

[Install]
WantedBy=timers.target
</code></pre>

<p>Note that this timer file does not specify a command to run. For that, we need
the corresponding foo.service file.</p>

<pre><code># /etc/systemd/system/foo.service
[Unit]
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=&quot;/usr/local/bin/example-timer.sh&quot;
ExecReload=/bin/kill $MAINPID
KillMode=mixed
KillSignal=SIGQUIT

[Install]
WantedBy=multi-user.target
</code></pre>

<p>The actual command we want scheduled is under the <code>[Unit]</code> section, specifically
the <code>ExecStart=</code> attribute.</p>

<h2 id="starting-a-timer">Starting a Timer</h2>

<p>To kick off the timer we defined above, run the following command.</p>

<pre><code>sudo systemctl start foo.timer
</code></pre>

<p>The timer should now be visible with <code>systemctl list-timers</code>. And the logs
for the associated service will be visible with journalctl.</p>

<pre><code>sudo journalctl --unit=foo
</code></pre>

<h2 id="further-reading">Further Reading</h2>

<p>We&rsquo;ve only seen the absolute minimum examples of timers and unit files. In
particular, there is another flavor of timer to run things on a predefined date,
like Christmas, your birthday, etc. This is possible with the <code>OnCalendar=</code>
attribute. <a href="https://www.freedesktop.org/software/systemd/man/systemd.timer.html">See the complete documentation</a>
for details.</p>

    </div>
  </div>

</body>

</html>
