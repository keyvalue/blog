
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-us">
<head>
  <link href="http://gmpg.org/xfn/11" rel="profile">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  
  <meta content="auth0, oauth, rust, rocket, jwt" name="keywords">
  

  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <title> A Rocket App with Auth0 Integration &middot; coleman.codes </title>

  
  <link rel="stylesheet" href="https://coleman.codes/css/poole.css">
  <link rel="stylesheet" href="https://coleman.codes/css/syntax.css">
  <link rel="stylesheet" href="https://coleman.codes/css/hyde.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface">

  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144-precomposed.png">
  <link rel="shortcut icon" href="/favicon.ico">

  
  <link href="" rel="alternate" type="application/rss+xml" title="coleman.codes" />

  
  <link rel="stylesheet" href="https://yandex.st/highlightjs/8.0/styles/default.min.css">
  <script src="https://yandex.st/highlightjs/8.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>


<body>

  <div class="sidebar">
  <div class="container sidebar-sticky">
    <div class="sidebar-about">
      <h1>Coleman</h1>
      <p class="lead"></p>
    </div>

    <ul class="sidebar-nav">
      <li><a href="/">Home</a> </li>
      
        <li><a href="/about/"> About </a></li>
      
    </ul>

  </div>
</div>


  <div class="content container">
    <div class="post">
      <h1>A Rocket App with Auth0 Integration</h1>
      <span class="post-date">Mon, May 7, 2018</span> 

<p>In this post we&rsquo;ll be walking through an <a href="https://github.com/dontlaugh/auth0-rocket-rust-example">example application</a> I&rsquo;ve sketched
out on GitHub. I threw this together because I didn&rsquo;t see anything similar
for Rust, Rocket, and basic Auth0 integration.</p>

<p>We will go through the process step by step, from configuring Auth0 in their
management console, to building, configuring, and running our application.</p>

<h2 id="auth0-configuration">Auth0 Configuration</h2>

<p>This example uses what Auth0 calls a &ldquo;Regular Web Application&rdquo;. Create one of
these in the <a href="https://manage.auth0.com/#/applications">management console</a>.</p>

<p><img src="/img/auth0_settings.png" alt="auth0 settings" /></p>

<p>Create a new application, or use an existing one. Our example Rust application
will need these application <strong>settings</strong> values, so keep them handy:</p>

<ul>
<li>Auth0 domain</li>
<li>Client ID</li>
<li>Client secret</li>
</ul>

<p>Since our application will do an authentication handshake with Auth0, we must
also enter some configuration into the Auth0 management console itself. This
ensures Auth0 will only accept and call back to URLs we explicitly specify (it&rsquo;s
all about security, after all).</p>

<p>First, add our localhost URL to the list of <strong>Allowed Callback URLs</strong>.</p>

<pre><code>https://localhost:8000/login
</code></pre>

<p>Scroll down and save your changes.</p>

<p>You can also take note of the values in <strong>Advanced Settings &gt; Endpoints</strong>. Auth0
sets these up for us, and our application will make use of these. Note that your
Auth0 domain is a component of these endpoints.</p>

<h2 id="install-rust-nightly">Install Rust (nightly)</h2>

<p>You&rsquo;ll need to install Rust, if you haven&rsquo;t already.</p>

<p>Rust comes in two main flavors, stable and nightly. Since our project dependencies
(including Rocket) require bleeding-edge compiler features, we must use <strong>nightly</strong>.
Don&rsquo;t let that scare you, managing versions of Rust is easy with <code>rustup</code>.</p>

<p>First, get rustup from <a href="https://rustup.rs/">https://rustup.rs/</a> and then use it to install
and use a nightly Rust compiler by default.</p>

<pre><code>rustup default nightly
</code></pre>

<p>You should now have Rust and <code>cargo</code>, the official Rust build tool and package
manager. You can think of cargo as <code>npm</code> or <code>pip</code>, but for Rust.</p>

<h2 id="build-the-rust-example">Build the Rust Example</h2>

<p>Clone our <a href="https://github.com/dontlaugh/auth0-rocket-rust-example">example project</a> to wherever you like to write code.</p>

<pre><code>git clone https://github.com/dontlaugh/auth0-rocket-rust-example
cd auth0-rocket-rust-example
</code></pre>

<p>Use cargo to build our project. If this is your first time using Rust, compilation
will take a long time. But don&rsquo;t worry! Build artifacts are cached, so subsequent
builds will be much faster.</p>

<pre><code>cargo build
</code></pre>

<h2 id="generate-self-signed-certificates">Generate Self-signed Certificates</h2>

<p>In our example, we provide a script to generate self-signed certificates to
serve our Rocket app locally over TLS. The script requires a working Go installation.
Run is like this:</p>

<pre><code>./generate-cert.sh
</code></pre>

<h2 id="configure-and-run-the-rust-example">Configure and Run the Rust Example</h2>

<p>Almost there. We need to provide our application the Auth0 configuration values
mentioned earlier. We&rsquo;ll use 1) a Rocket.toml file and 2) an environment variable.</p>

<p>First, make a copy of the example Rocket.toml file.</p>

<pre><code>cp Rocket.toml.example Rocket.toml
</code></pre>

<p>In Rocket.toml add your Auth0 <strong>client id</strong>, <strong>domain</strong>, and the localhost
<strong>redirect uri</strong> to the <code>[global]</code> configuration stanza. Be sure to quote the
strings.</p>

<pre><code class="language-toml">[global]
client_id = &quot;YOUR_AUTH0_CLIENT_ID&quot;
redirect_uri = &quot;http://localhost:8000/login&quot;
auth0_domain = &quot;YOUR_AUTH0_DOMAIN&quot;

## only if you want to use TLS
[global.tls]
certs = &quot;cert.pem&quot;
key = &quot;key.pem&quot;
</code></pre>

<p>Again, note that here we are assuming you&rsquo;re using TLS certs generated earlier.</p>

<p>Next, set <code>AUTH0_CLIENT_SECRET</code> in your shell environment.</p>

<pre><code>export AUTH0_CLIENT_SECRET=&quot;very-secret-value&quot;
</code></pre>

<p>We&rsquo;re ready to run our app! Execute <code>cargo run</code> and open a browser at
<a href="http://localhost:8000/">http://localhost:8000/</a> You will be redirected to the login link.</p>

<p><img src="/img/login_with_auth0.png" alt="picture of login link" /></p>

<p>Click the link, and you&rsquo;ll be redirected to an Auth0-hosted login page. Only
valid users of your app can login here. Upon successful login, you&rsquo;ll be
redirected back to localhost.</p>

<p><img src="/img/guarded_route.png" alt="picture of guarded route" /></p>

<h2 id="code-walkthrough">Code Walkthrough</h2>

<p>Great, it works! But how?</p>

<h2 id="routes-in-rocket">Routes in Rocket</h2>

<p>Rocket uses custom attributes to specify it&rsquo;s routes. To me, they resemble the
route decorators from Flask. Like Flask, Rocket routes are just regular functions.</p>

<p>Here we see the <code>#[get(&quot;/login&quot;)]</code> attribute decorating the <code>login</code> function.
This will handle GET requests to the /login route.</p>

<pre><code class="language-rust">/// Our login link.
#[get(&quot;/login&quot;)]
fn login() -&gt; Markup {
    html!{
        head {
            title &quot;Login | Auth0 Rocket Example&quot;
            link rel=&quot;stylesheet&quot; href=&quot;static/css/style.css&quot;;
        }
        body {
            a class=&quot;login&quot; href=&quot;/auth0&quot; &quot;Login With Auth0!&quot;
        }
    }
}
</code></pre>

<p>These custom attributes are a bleeding-edge feature in Rust, and one of the
reasons we need the nightly compiler to use Rocket.</p>

<p>The <code>Markup</code> type and the <em>excited!</em>-looking <code>html!</code> macro are from <a href="https://github.com/lfairy/maud">maud</a>, a
library that allows us to write dynamic templates as a DSL directly in our Rust
code. There isn&rsquo;t much else to say about maud. The DSL is very HTML-like. Its
integration with Rocket is really great, and we don&rsquo;t have to ship any static
HTML files. It&rsquo;s all generated at compile time.</p>

<h2 id="rocket-s-request-guards">Rocket&rsquo;s Request Guards</h2>

<p>This is a tutorial about protecting parts of our site behind from unauthenticated
users. Rocket lets us do this with <em>request guards</em>. A request guard in
Rocket is simply a Rust type that implements the <code>FromRequest</code> trait. We can
then use these types in the function signature of our routes. The <code>FromRequest</code>
code, along with the url, will determine whether the route is served.</p>

<p>In our example, we have two handlers for the same <code>&quot;/&quot;</code> route, but they take
different parameters. Crucially, our <code>User</code> type is a request guard.</p>

<pre><code class="language-rust">#[get(&quot;/&quot;)]
fn home(user: User) -&gt; Markup { ... }

fn home_redirect() -&gt; Redirect {
    Redirect::to(&quot;/login&quot;)
}
</code></pre>

<p>Since our <code>User</code> type is a request guard, the <code>home</code> function will only be
routed to if the associated request guard code succeeds.</p>

<p>Under the hood, our implementation of this request guard checks the user&rsquo;s cookie
for a hashed jwt, and ensures that cookie value matches a hashed jwt in our database. If
any part of this fails, <code>home</code> will not be evaluated, and the request will be <em>forwarded</em>
to the next matching route. In our example, that&rsquo;s the <code>home_redirect</code> function,
which immediately redirects to our login link.</p>

<p>Other frameworks use HTTP middleware for this sort of thing. In Rocket, we&rsquo;re
encouraged to use request guards. This is a nice feature, because we could
potentially specify many request guards for a route, and adding or removing them
is a simpler process than refactoring middleware.</p>

<h2 id="rocket-s-managed-state">Rocket&rsquo;s Managed State</h2>

<p>Routes need to make use of shared resources, like database connections. Rocket
requires that we explicitly declare these dependencies with the <code>manage</code> method
at launch. Here is a snippet of code from our <code>main</code> function where we open a
<a href="https://github.com/spacejam/sled">sled database</a> and bring it under Rocket&rsquo;s
state management.</p>

<pre><code class="language-rust">let db: DB = {
    let config = sled::ConfigBuilder::new().path(&quot;.data&quot;).build();
    Arc::new(sled::Tree::start(config).unwrap())
};
rocket::ignite().mount(...).manage(db).attach(...).launch();
                        //  ^ call to manage here
</code></pre>

<p><em>Note: an Arc in Rust is a smart pointer that lets us share resources between
threads. Our webserver is multi-threaded, so we must wrap our db handle with an Arc.</em></p>

<p>Once a resource is managed, we can bring it into the scope of any of our routes
by adding its type, wrapped with <code>State</code>, to the parameters of a handler function.
When Auth0 calls us back during an authentication handshake, we need access to
two bits of shared state: the database pointer and a settings object we construct
at launch.</p>

<pre><code class="language-rust">#[get(&quot;/callback?&lt;callback_params&gt;&quot;)]
fn auth0_callback(
    callback_params: CallbackParams,
    mut cookies: Cookies,
    db: State&lt;DB&gt;,
    settings: State&lt;AuthSettings&gt;,
) -&gt; Result&lt;Redirect, Status&gt; { ... }
</code></pre>

<p>In the <code>auth0_callback</code> function, both <code>db</code> and <code>settings</code> are passed into our
function&rsquo;s scope automatically. We can use these resources safely for the
duration of our function.</p>

<h2 id="conclusion">Conclusion</h2>

<p>Many docs are <a href="https://github.com/dontlaugh/auth0-rocket-rust-example#helpful-docs-from-auth0">linked from the README</a> of this example project&rsquo;s repo, all from
the Auth0 community.</p>

<p>If you&rsquo;re new to Rust, jumping straight into web servers and authentication can
be daunting. I hope that I&rsquo;ve provided something that builds, runs, and gets
you started. If anything is broken, or if you have better security recommendations
please <a href="https://github.com/dontlaugh/auth0-rocket-rust-example/issues/new">open an issue</a>.</p>

    </div>
  </div>

</body>

</html>
