
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-us">
<head>
  <link href="http://gmpg.org/xfn/11" rel="profile">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  

  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <title> A basic tmux bash script explained &middot; coleman.codes </title>

  
  <link rel="stylesheet" href="https://coleman.codes/css/poole.css">
  <link rel="stylesheet" href="https://coleman.codes/css/syntax.css">
  <link rel="stylesheet" href="https://coleman.codes/css/hyde.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface">

  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144-precomposed.png">
  <link rel="shortcut icon" href="/favicon.ico">

  
  <link href="" rel="alternate" type="application/rss+xml" title="coleman.codes" />

  
  <link rel="stylesheet" href="https://yandex.st/highlightjs/8.0/styles/default.min.css">
  <script src="https://yandex.st/highlightjs/8.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>


<body>

  <div class="sidebar">
  <div class="container sidebar-sticky">
    <div class="sidebar-about">
      <h1>Coleman</h1>
      <p class="lead"></p>
    </div>

    <ul class="sidebar-nav">
      <li><a href="/">Home</a> </li>
      
        <li><a href="/about/"> About </a></li>
      
    </ul>

  </div>
</div>


  <div class="content container">
    <div class="post">
      <h1>A basic tmux bash script explained</h1>
      <span class="post-date">Sat, May 16, 2015</span> 

<p>Since I&rsquo;ve been writing more Go, I&rsquo;ve been using a lot more
plain text editors such as vim and Sublime Text. I tried to
use an IDE for a while (IntelliJ with Go plugins), but it
felt antithetical to the raw, low-level nature of Go. Also,
the tooling wouldn&rsquo;t let me set my <code>$GOPATH</code> in an easy way.</p>

<p>So, back to text editors and shells for me.</p>

<h3 id="scripting-tmux">Scripting tmux</h3>

<p>Many people recommend the <strong>tmuxinator</strong> Ruby gem to configure
tmux sessions, but I&rsquo;m trying to avoid a dependency on Ruby, so
I&rsquo;m going to use raw bash scripts for launching tmux sessions.</p>

<p>I don&rsquo;t need to do anything too complicated. Here are my goals:</p>

<ul>
<li>Launch a session with a name</li>
<li>Create two windows, each with names</li>
<li>In the first window (at index <code>0</code>) split into 3 panes with a sideways T shape (like <code>|-</code>)</li>
<li>In the second window (index <code>1</code>), split into panes with a vim session on the left and a vim session on the right</li>
<li>Back in the first window, send a <code>tail</code> command to the upper-right pane, to start monitoring a specific log file</li>
<li>Attach to the session, viewing the first window</li>
</ul>

<p>Here is the bash script that achieves this:</p>

<pre><code class="language-bash">#!/bin/sh

tmux new-session -d -s dev-session
tmux select-window -t 0
tmux rename-window Shells
tmux new-window -n 'Editors' 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
tmux select-window -t dev-session:Shells
tmux split-window -h 'tail -f $GOPATH/src/github.com/anxiousmodernman/dirz/dirz.log'
tmux split-window -v
tmux select-window -t dev-session:Editors
tmux split-window -h 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
tmux select-window -t dev-session:Shells
tmux -2 attach-session -t dev-session
</code></pre>

<h3 id="breaking-it-down">Breaking it down</h3>

<p>First, we make a new session</p>

<pre><code class="language-bash">tmux new-session -d -s dev-session
</code></pre>

<p>Next, we can target the first (and only) existing default window in the session,
and rename the window to &ldquo;Shells&rdquo;.</p>

<pre><code class="language-bash">tmux select-window -t 0
tmux rename-window Shells
</code></pre>

<p>Then, spawn a new window. Give it the name &lsquo;Editors&rsquo; by passing the <code>-n</code> flag, and execute <code>vim</code> as the command. The easiest way (it seems to me)
to get tmux to run a command in a window or pane is to pass it as the final argument as a string <strong>during the creation of that window or pane</strong>.</p>

<pre><code class="language-bash">tmux new-window -n 'Editors' 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
</code></pre>

<p>Switch back to the &ldquo;Shells&rdquo; window with the <code>select-window</code> command, followed by the <code>-t</code> (target) flag, and then tmux-ish namespace path with the format
<code>session-name:window-name</code>. Then I immediately split this window into left-and-right panes with <code>split-window -h</code>. Note that the new pane, which will
appear on the <em>right</em> has focus after it is split, and to achieve my goal of tailing a file I pass that new pane the <code>tail</code> command with arguments.</p>

<pre><code class="language-bash">tmux select-window -t dev-session:Shells
tmux split-window -h 'tail -f $GOPATH/src/github.com/anxiousmodernman/dirz/dirz.log'
</code></pre>

<p>Then, do another split. This time a <code>-v</code> split <em>while the right pane has focus</em>. This will give us two panes on the right half of the screen
and one tall pane in the left, achieving the sideways T shape (<code>|-</code>) I want.</p>

<pre><code class="language-bash">tmux split-window -v
</code></pre>

<p>We are done with the &lsquo;Shells&rsquo; window. Next we switch back to &lsquo;Editors&rsquo;, and do our final split, again running vim with the same directory as a target.</p>

<pre><code class="language-bash">tmux select-window -t dev-session:Editors
tmux split-window -h 'vim $GOPATH/src/github.com/anxiousmodernman/dirz'
</code></pre>

<p>Since vim targeted a directory both times, the default nerd tree directory view is what we see when we first view the &lsquo;Editors&rsquo; window.</p>

<p>Finally, we focus back on &lsquo;Shells&rsquo;, and attach to our newly-created tmux session!</p>

<pre><code class="language-bash">tmux select-window -t dev-session:Shells
tmux -2 attach-session -t dev-session
</code></pre>

<p>You can change the paths in my shell script example and give it a shot.</p>

    </div>
  </div>

</body>

</html>
