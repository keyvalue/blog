
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-us">
<head>
  <link href="http://gmpg.org/xfn/11" rel="profile">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  

  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <title> Notes on a Language Agnostic Project Template DSL &middot; coleman.codes </title>

  
  <link rel="stylesheet" href="https://coleman.codes/css/poole.css">
  <link rel="stylesheet" href="https://coleman.codes/css/syntax.css">
  <link rel="stylesheet" href="https://coleman.codes/css/hyde.css">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface">

  
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144-precomposed.png">
  <link rel="shortcut icon" href="/favicon.ico">

  
  <link href="" rel="alternate" type="application/rss+xml" title="coleman.codes" />

  
  <link rel="stylesheet" href="https://yandex.st/highlightjs/8.0/styles/default.min.css">
  <script src="https://yandex.st/highlightjs/8.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>

</head>


<body>

  <div class="sidebar">
  <div class="container sidebar-sticky">
    <div class="sidebar-about">
      <h1>Coleman</h1>
      <p class="lead"></p>
    </div>

    <ul class="sidebar-nav">
      <li><a href="/">Home</a> </li>
      
        <li><a href="/about/"> About </a></li>
      
    </ul>

  </div>
</div>


  <div class="content container">
    <div class="post">
      <h1>Notes on a Language Agnostic Project Template DSL</h1>
      <span class="post-date">Mon, Mar 23, 2015</span> 

<p>Mature languages all have build tools, and many have requirements around where files should be located.
Some systems, like <strong>Gulp</strong> and <strong>Grunt</strong> (JavaScript), simply require a config file to be present in the project root, and the rest of the structure is up to you. Others, like <strong>sbt</strong> and <strong>Maven</strong> (Scala and Java), are <a href="http://www.scala-sbt.org/0.13/tutorial/Directories.html">more opinionated in their recommendations</a>.</p>

<p>When learning a new framework or language, one of the things I like to do is plan out a structure of the
project. Sometimes I&rsquo;ll want to do this even before writing any code. I believe it encourages good design, and gets me thinking about packages and apis before I get myself into too much trouble.</p>

<p>In practical terms, this means creating some directories and files. This is especially the case with the wild-west anarchy of JavaScript projects</p>

<h3 id="a-modest-idea">A modest idea</h3>

<p>To aid in this layout/design process, I&rsquo;d like help from a utility. Here is how it would work. I want to be able to write a simple text file to disk, execute a command that reads the file, and have a new project&rsquo;s directory structure generated automatically. The file I have in mind would look like this (a React.js project example):</p>

<pre><code class="language-markdown">/my-project
  /app
    /components
      header.jsx
      footer.jsx
      listItem.jsx
    /services
      core.js
      util.js
    /public
    /views
      index.html
    app.jsx
  main.js
  README.md
  Gulpfile.js
  package.json
  .gitignore
</code></pre>

<p>That&rsquo;s 18 lines of text. Sketching it out in vim or Sublime Text would be relatively quick. After the file (named <code>project.dirz</code>) is written to disk, I&rsquo;d like to execute a command (provsionally called <code>dirz</code>) like this:</p>

<pre><code>$ dirz project.dirz
</code></pre>

<p>The command would read the file and automatically create the corresponding directory structure. The other files would be created as empty files, as if I had run the <code>touch</code> command for each of them. Effectively, the <code>project.dirz</code> file contains a domain specific language (DSL) for powering the creation of project skeletons.</p>

<p>Empty files and directories are fine for many things, but as an extension to the language, I would like to be able to provide an optional template for any of the files. For example, a lot of <code>packages.json</code> files look the same, and maybe I always want to provide the same defaults. In the <code>dirz</code> language, I could provide a template just for this file by adding a template name after that filename like so:</p>

<pre><code class="language-markdown">/my-project
  /app
    /components
      header.jsx
      footer.jsx
      listItem.jsx
    /services
      core.js
      util.js
    /public
    /views
      index.html
    app.jsx
  main.js
  README.md
  Gulpfile.js
  package.json  package-json.dirz  # comments
  .gitignore
</code></pre>

<p>But we could automate even more. Manually typing a frequently-used template name is tedious, so I could instead pass a flag to the <code>dirz</code> command that would apply common templates based on filenames. Something like:</p>

<pre><code>$ dirz --templates gulp-react
</code></pre>

<p>The specification for the templates that apply to the <code>gulp-react</code> project type could live in a global cache managed by the <code>dirz</code> utility. Perhaps this particular spec would apply defaults to <strong>README.md</strong>, <strong>Gulpfile.js</strong>, <strong>package.json</strong>, and <strong>.gitignore</strong>. Using the <code>templates</code> flag with a project type would be the (more reusable) equivalent of manually typing out the following file and executing it directly with <code>dirz</code>:</p>

<pre><code class="language-markdown">/my-project
  /app
    /components
      header.jsx
      footer.jsx
      listItem.jsx
    /services
      core.js
      util.js
    /public
    /views
      index.html
    app.jsx
  main.js
  README.md     readme-standard.dirz
  Gulpfile.js   gulpfile.dirz
  package.json  package-json.dirz  
  .gitignore    node-gitignore.dirz
</code></pre>

<p>The <code>templates</code> flag would only be concerned with applying templates to files. It would not apply any directory structure. That is still controlled by the DSL. However, if you want to reuse a whole structure, templates, directories, and all, I&rsquo;m thinking you can just pass the project name like this:</p>

<pre><code>$ dirz gulp-react
</code></pre>

<p>And the whole project would be created. Directory structure, templates, and named blank files.</p>

<h3 id="why-not-use-git">Why not use git?</h3>

<p>You might be wondering why this is better than simply cloning a &ldquo;seed&rdquo; project from GitHub. I have two answers.</p>

<ol>
<li>Many &ldquo;seed&rdquo; projects are examples with working code, and it discourages the user from doing their own thing, and going through the necessary steps to create a project that successfully builds and runs. Seed projects also create a lot of cruft, and that means mental clutter, too.</li>
<li>Cloning seed projects means hunting around on GitHub. That&rsquo;s totally fine, but there are a lot of examples out there that are just blog posts or <a href="http://stackoverflow.com/questions/11522151/typical-angular-js-workflow-and-project-structure-with-python-flask">Stack Overflow posts</a>. Having a tool like <code>dirz</code> lets you quickly sketch out a project with ideas borrowed from multiple sources.</li>
</ol>

<p>My plan is to write the tool in Go, thanks to its built in template language, speed, and ability to compile to portable executables on just about any platform.</p>

    </div>
  </div>

</body>

</html>
